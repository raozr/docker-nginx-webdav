FROM alpine:latest

MAINTAINER Kevin Allioli <kevin@linit.io>

# Define environment variables
ENV NGINX_VERSION nginx-1.20.1
ENV S6_VERSION 2.2.0.3

# Add dependencies (S6 and scripts)
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_VERSION}/s6-overlay-amd64.tar.gz /tmp/
RUN tar xzf /tmp/s6-overlay-amd64.tar.gz -C /
COPY ./files/10-adduser /etc/cont-init.d/

# Install packages and compiling NGINX from sources
RUN apk --update add openssl-dev pcre-dev zlib-dev wget build-base git libxslt-dev shadow && \
    mkdir -p /tmp/src && \
    cd /tmp/src && \
    wget http://nginx.org/download/${NGINX_VERSION}.tar.gz && \
    git clone https://github.com/arut/nginx-dav-ext-module.git && \
    tar -zxf ${NGINX_VERSION}.tar.gz && \
    cd /tmp/src/${NGINX_VERSION} && \
    ./configure \
        --with-http_ssl_module \
        --with-http_gzip_static_module \
        --with-http_dav_module --add-module=/tmp/src/nginx-dav-ext-module \
        --prefix=/usr/share/nginx \
        --sbin-path=/usr/sbin/nginx \
        --modules-path=/usr/lib/nginx/modules \
        --conf-path=/etc/nginx/nginx.conf \
        --pid-path=/run/nginx.pid \
        --lock-path=/var/lock/nginx.lock \
        --prefix=/etc/nginx \
        --http-log-path=/var/log/nginx/access.log \
        --error-log-path=/var/log/nginx/error.log && \
    make && \
    make install && \
    apk del build-base git && \
    rm -rf /tmp/src && \
    rm -rf /var/cache/apk/* && \
    mkdir /etc/nginx/sites-enabled && \
    mkdir /etc/nginx/security && \
    mkdir /srv/data && \
    rm /etc/nginx/nginx.conf 

# Add default config for NGINX and creating user
COPY ./files/nginx.conf /etc/nginx/nginx.conf
RUN addgroup -g 1000 -S abc && \
    adduser -u 1000 -S abc -G abc

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log

VOLUME ["/etc/nginx/sites-enabled"]
VOLUME ["/etc/nginx/security"]
VOLUME ["/srv/data/"]
WORKDIR /etc/nginx

EXPOSE 80
STOPSIGNAL SIGTERM
ENTRYPOINT ["/init"]
CMD ["nginx", "-g", "daemon off;"]
